﻿using UnityEngine;
using System.Collections;

public class FoundautumnTree : MonoBehaviour {

	public bool Onautumn;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if ((this.tag=="trackedTree")&(Onautumn==true)) {
			renderer.enabled = true;
			OnTrackingFound();

		} 
		else
		{
			renderer.enabled=false;
			OnTrackingLost();

		}
	}

	private void OnTrackingFound()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);

		
		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}
		


	}
	private void OnTrackingLost()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);

		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}
		

	}
}

