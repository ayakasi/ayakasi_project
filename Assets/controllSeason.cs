﻿using UnityEngine;
using System.Collections;

public class controllSeason : MonoBehaviour
{
	public GameObject SPRtree;
	public GameObject SMRtree;
	public GameObject AMNtree;
	public GameObject WNTtree;
	public int season;

	// Use this for initialization
	void Start ()
	{
		season = 0;
	}
	// Update is called once per frame
	void Update ()
	{


	FoundspringTree SpringScript = SPRtree.GetComponent<FoundspringTree>();
		FoundsummerTree SummerScript = SMRtree.GetComponent<FoundsummerTree>();
		FoundautumnTree AutumnScript = AMNtree.GetComponent<FoundautumnTree>();
		FoundwinterTree WinterScript = WNTtree.GetComponent<FoundwinterTree>();

		switch(season){
		case 0:
			SpringScript.OnSpring = true;
			SummerScript.Onsummer = false;
			AutumnScript.Onautumn = false;
			WinterScript.Onwinter = false;
			break;
		case 1:
			SummerScript.Onsummer = true;
			SpringScript.OnSpring = false;

			AutumnScript.Onautumn = false;
			WinterScript.Onwinter = false;
			break;
		case 2:
			SpringScript.OnSpring = false;
			SummerScript.Onsummer = false;
			AutumnScript.Onautumn = true;
			WinterScript.Onwinter = false;
			break;
		case 3:
			SpringScript.OnSpring = false;
			SummerScript.Onsummer = false;
			AutumnScript.Onautumn = false;
			WinterScript.Onwinter = true;
			break;
		default:
			break;


		}
	}
}