﻿using UnityEngine;
using System.Collections;

public class FoundsummerTree : MonoBehaviour {

	public bool Onsummer;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if ((this.tag=="trackedTree")&(Onsummer==true)) {
			renderer.enabled = true;
			OnTrackingFound();
			
		} 
		else
		{
			renderer.enabled=false;
			OnTrackingLost();
			
		}
	}
	
	private void OnTrackingFound()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		
		
		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}
		
		
		
	}
	private void OnTrackingLost()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}
		
		
	}
}