﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AyakasiMain : MonoBehaviour
{
	//以下変数の定義など
	public enum MainStatus
	{
		TITLE,
		ROULETTE,
		FOUNDAYAKASI,
		FOUNDITEM1,
		FOUNDITEM2,
		SUMAURA,
		BUTTLE,
		RESULT,
		BACK,
		TOTALRESULT,
		NOACTION,
		SUDDENDEATH}
	;//ここまでメインステータスを定義

	public int Nmatch;//何回戦目か
	public int WinOrLost;//勝敗処理（０＝ドロー、１，２はプレイヤー１，２の勝利　）
	public int p1_winP, p2_winP;//(１，２プレイヤーの勝敗数)
	public string p1_result1,p1_result2,p1_result3,p2_result1,p2_result2,p2_result3;//勝敗文字列（勝ち、負け、わけ）

	public int Season_val;//場のシーズン（０＝春～３＝冬）
	public string Season;//シーズンの文字列
	public int p1season, p2season;//キャラ属性（０＝春～３＝冬）
	public int p1ItemSeason1, p1ItemSeason2, p2ItemSeason1, p2ItemSeason2;//お供え属性（０＝春～３＝冬）

	public string player1, player2;//プレイヤーの出したayakasiの名前
	public string p1item1, p1item2, p2item1, p2item2;//アイテムカード名

	public int p1ItemId1, p1ItemId2, p2ItemId1, p2ItemId2;//アイテムカードIDポイントと特殊カード認識
	public int AyakasiAura1,AyakasiAura2;//あやかしのパワー
	public int p1ItemAura1,p1ItemAura2,p2ItemAura1,p2ItemAura2;//アイテムポイント
	public int aura1, aura2;//総合あやかしパワー
	public int sp1, sp2;//シーズンパワー


	public MainStatus status;//メインステータス、この内容で処理振り分けを行う

	public RawImage TitleImage;//タイトル画面
	public RawImage ResultImage;//結果画面
	public RawImage BattleBackImage;//通常背景
	public RawImage skip1image,twoskipimage,randomimage,Seasoncomp;
	public Button MainBt;//メインボタンのオブジェクト
	public Button reset;//リセットボタン、GameEnd関数へ
	public Button resetThisGame;//リターンボタン、resetThisGame関数へ


	public GameObject SPRIcon;//春アイコン
	public GameObject SMRIcon; //夏アイコン
	public GameObject AMNIcon; //秋アイコン
	public GameObject WNTIcon; //冬アイコン

	public Text msgText;//メッセージボックスの文字列
	public Text bttext;//メインボタン文字
	public Text p1text;//１ｐカード情報欄
	public Text p2text;//
	public Text AuraText1;//あやかしパワー表示のテキスト欄
	public Text AuraText2;
	public Text forDebug;//デバッグ用お供えカードの認識している枚数
	public Text p1_1text, p1_2text, p1_3text, p2_1text, p2_2text, p2_3text;
	public Text p1ayakasiA,P1Item1A,P1Item2A,p2ayakasiA,P2Item1A,P2Item2A,SeasonAR1,SeasonAR2;//カードポイント表示欄テクスト
	public GameObject ayakasi1, ayakasi2;
	public GameObject item1, item2;
	public GameObject SPRtree;//春の木オブジェクト
	public GameObject SMRtree;//夏の木オブジェクト
	public GameObject AMNtree;//秋の木オブジェクト
	public GameObject WNTtree;//冬の木オブジェクト
	public GameObject enban;//ルーレットの文字盤
	public Animation roulette_anime;// ルーレットのアニメ
	public Animation oneSkipAnim1;

	// 初期設定
	void Start ()
	{

		player1 = "";
		p1text.text = "";
		player2 = "";
		p2text.text = "";
		p1_1text.text = "";
		p1_2text.text = "";
		p1_3text.text = "";
		p2_1text.text = "";
		p2_2text.text = "";
		p2_3text.text = "";
		AuraText1.text = "";
		AuraText2.text = "";
		p1ayakasiA.text = "";
		P1Item1A.text = "";
		P1Item2A.text = "";
		p2ayakasiA.text = "";
		P2Item1A.text = "";
		P2Item2A.text = "";
		SeasonAR1.text = "";
		SeasonAR2.text = "";
		Season_val = 0;
		p1ItemSeason1 = 4; p1ItemSeason2 = 4; p2ItemSeason1 = 4; p2ItemSeason2=4;
		sp1 = 0;
		sp2 = 0;
		AyakasiAura1 = 0;
		AyakasiAura2 = 0;
		p1_winP = 0;
		p2_winP = 0;
		p1ItemAura1 = 0;
		p1ItemAura2 = 0;
		p2ItemAura1 = 0;
		p2ItemAura2 = 0;
		

		status = MainStatus.TITLE;//メインステータスを「タイトル」へ
		MainBt.interactable = false;
		skip1image.enabled = false;
		Seasoncomp.enabled = false;
		//SeasonIcon = Icon.GetComponent<Image> ();
		//SeasonIcon.= false;
	

		//VSボタンの非表示
		foreach (Transform child in this.transform){
			if(child.name == "BtBattle"){
				Button buttontmp = child.gameObject.GetComponent<Button>();
				buttontmp.gameObject.SetActive (false);
			}
		}
	}
	
	// Update is called once per frame
	//ステータス確認して、表示処理
	void Update ()
	{

		switch (status) {
		case MainStatus.TITLE:
			Nmatch = 1;
			msgText.text = "ゲームを始めます！";

			TitleImage.enabled = true;
			TitleImage.transform.SetAsLastSibling();
			BattleBackImage.enabled = false;
			ResultImage.enabled = false;

			MainBt.interactable = true;
			GameObject dialogMC = GameObject.Find("dialog");
			dialogMC.transform.SetAsLastSibling();

		

			bttext.text = "START";
			break;

		case MainStatus.ROULETTE:
			msgText.text = "ルーレットで季節を決めます";
			
			MainBt.interactable = true;
			bttext.text = "roulette!";
			break;

		case MainStatus.FOUNDAYAKASI:
			MainBt.interactable = true;
			resetThisGame.interactable=true;
			BattleBackImage.enabled = true;
			p1_1text.text = "";
			p1_2text.text = "";
			p1_3text.text = "";
			p2_1text.text = "";
			p2_2text.text = "";
			p2_3text.text = "";
			AuraText1.text="";
			AuraText2.text="";
			msgText.text = "あやかしカードオープン";
			bttext.text = "あやかし召喚";

			//ShowSeason();

			break;

		case MainStatus.FOUNDITEM1:
			MainBt.interactable = true;
			resetThisGame.interactable=true;
			bttext.text = "お供え";
			break;

		case MainStatus.FOUNDITEM2:
			MainBt.interactable = true;
			resetThisGame.interactable=true;
			bttext.text = "お供えおかわり";
			break;

		case MainStatus.SUMAURA:
			MainBt.interactable = true;
			resetThisGame.interactable=false;
			bttext.text = "完了";
			break;

		case MainStatus.RESULT:
			MainBt.interactable = true;
			resetThisGame.interactable=false;
			//msgText.text = "○○のかちです。";
			bttext.text = "戦績表示";
			break;

		case MainStatus.BACK:
			MainBt.interactable = true;
			resetThisGame.interactable=false;
			bttext.text = "つぎへ";
			break;

		case MainStatus.TOTALRESULT:
			MainBt.interactable = true;
			resetThisGame.interactable=false;
			bttext.text = "終了";
			break;

		case MainStatus.NOACTION:
		
			break;

		default:
			msgText.text = "defa";
			resetThisGame.interactable=false;
			break;
		}
	}



	//メインボタンクリック処理（ステータスで各処理の関数を呼ぶ）

	public void click_button ()
	{
		switch (status) {
		case MainStatus.TITLE:
			TitleImage.enabled = false;
			MainBt.interactable = false;
			bttext.text = "";
			status = MainStatus.ROULETTE;
			break;

		case MainStatus.ROULETTE:
			MainBt.interactable = false;
			bttext.text = "";
			msgText.text = "";
			Season_val = UnityEngine.Random.Range (0, 4);
			Roulette();
			break;

		case MainStatus.FOUNDAYAKASI:

			MainBt.interactable = false;
			bttext.text = "";
			ReadAyakasi ();
			break;

		case MainStatus.FOUNDITEM1:
			MainBt.interactable = false;
			bttext.text = "";
			readitem1 ();
			break;

		case MainStatus.FOUNDITEM2:
			MainBt.interactable = false;
			bttext.text = "";
			readitem2 ();
			break;

		case MainStatus.SUMAURA:
			MainBt.interactable = false;
			bttext.text = "";
			SumAura ();

			//VSボタン非表示（不要分部）
			//foreach (Transform child in this.transform){
				//if(child.name == "BtBattle"){
				//	Button buttontmp = child.gameObject.GetComponent<Button>();
				//	buttontmp.gameObject.SetActive (false);
				//}
			//}
			break;
		
		case MainStatus.RESULT:
			MainBt.interactable = false;
			ResultImage.enabled = true;
			ResultImage.transform.SetAsLastSibling();
			GameObject dialogMC = GameObject.Find("dialog");
			dialogMC.transform.SetAsLastSibling();
			GameObject marubatuMC = GameObject.Find("marubatu");
			marubatuMC.transform.SetAsLastSibling();
			bttext.text = "";
			ShowResult ();
			break;

		case MainStatus.BACK:
			ResultImage.enabled = false;
			bttext.text = "";
			GoBack ();
			break;
		
		case MainStatus.TOTALRESULT:
			MainBt.interactable = true;
			bttext.text = "";
			GameEnd();
			break;

		default:

			break;
		}

	
	}

	//以下　関数

//季節の木表示処理
	void ShowSeason ()
	{
		FoundspringTree SpringScript = SPRtree.GetComponent<FoundspringTree> ();
		FoundsummerTree SummerScript = SMRtree.GetComponent<FoundsummerTree> ();
		FoundautumnTree AutumnScript = AMNtree.GetComponent<FoundautumnTree> ();
		FoundwinterTree WinterScript = WNTtree.GetComponent<FoundwinterTree> ();

		switch (Season_val) {
		case 0:

			Season = "春";
			SpringScript.OnSpring = true;
			SummerScript.Onsummer = false;
			AutumnScript.Onautumn = false;
			WinterScript.Onwinter = false;
			  
			SPRIcon.SetActive(true);
			SMRIcon.SetActive(false);
			AMNIcon.SetActive(false);
			WNTIcon.SetActive(false);

			break;
		case 1:

			Season = "夏";
			SummerScript.Onsummer = true;
			SpringScript.OnSpring = false;
			
			AutumnScript.Onautumn = false;
			WinterScript.Onwinter = false;
			SPRIcon.SetActive(false);
			SMRIcon.SetActive(true);
			AMNIcon.SetActive(false);
			WNTIcon.SetActive(false);
			break;
		case 2:

			Season = "秋";
			SpringScript.OnSpring = false;
			SummerScript.Onsummer = false;
			AutumnScript.Onautumn = true;
			WinterScript.Onwinter = false;
			SPRIcon.SetActive(false);
			SMRIcon.SetActive(false);
			AMNIcon.SetActive(true);
			WNTIcon.SetActive(false);
			break;
		case 3:

			Season = "冬";
			SpringScript.OnSpring = false;
			SummerScript.Onsummer = false;
			AutumnScript.Onautumn = false;
			WinterScript.Onwinter = true;
			SPRIcon.SetActive(false);
			SMRIcon.SetActive(false);
			AMNIcon.SetActive(false);
			WNTIcon.SetActive(true);
			break;

		}
		msgText.text = "季節は「" + Season + "」です！";

	}

	void SeasonIconOff ()//シーズンアイコンを非表示
	{
	SPRIcon.SetActive(false);
	SMRIcon.SetActive(false);
	AMNIcon.SetActive(false);
	WNTIcon.SetActive(false);
	}

	//ルーレット表示
	void Roulette ()
	{
		enban.renderer.enabled = true;
		roulette_anime = enban.GetComponent<Animation> ();
		switch (Season_val) {
		case 0:
			roulette_anime.Play ("spring");
			break;
		case 1:
			roulette_anime.Play ("summer");
			break;
		case 2:
			roulette_anime.Play ("autumn");
			break;
		case 3:
			roulette_anime.Play ("winter");
			break;

		}
		status = MainStatus.NOACTION;
		StartCoroutine ("wait4enban");
	}

	
	//あやかしを読み込む
	void ReadAyakasi ()
	{
		Vector3 ptemp0, ptemp1;
		ptemp0 = new Vector3 (0f, 0f, 0f);
		ptemp1 = new Vector3 (0f, 0f, 0f);
		GameObject[] TrcdAyakasi;
		TrcdAyakasi = GameObject.FindGameObjectsWithTag ("tracked");

		if (TrcdAyakasi.Length == 2) {
			ptemp0 = Camera.main.WorldToViewportPoint (TrcdAyakasi [0].transform.position);
			ptemp1 = Camera.main.WorldToViewportPoint (TrcdAyakasi [1].transform.position);
			if (ptemp0.x < ptemp1.x) {
				ayakasi1 = TrcdAyakasi [0];
				ayakasi2 = TrcdAyakasi [1];
				p1text.text = ayakasi1.name; 
				p2text.text = ayakasi2.name; 
				p1season = TrcdAyakasi [0].GetComponent<ayakasi_id> ().attribute;
				p2season = TrcdAyakasi [1].GetComponent<ayakasi_id> ().attribute;
				AyakasiAura1 = TrcdAyakasi [0].GetComponent<ayakasi_id> ().ID;
				AyakasiAura2 = TrcdAyakasi [1].GetComponent<ayakasi_id> ().ID;
			

			} else {
				ayakasi1 = TrcdAyakasi [1];
				ayakasi2 = TrcdAyakasi [0];
			
				p1text.text = ayakasi1.name;
				p2text.text = ayakasi2.name;
				p1season = TrcdAyakasi [1].GetComponent<ayakasi_id> ().attribute;
				p2season = TrcdAyakasi [0].GetComponent<ayakasi_id> ().attribute;
				AyakasiAura1 = TrcdAyakasi [1].GetComponent<ayakasi_id> ().ID;
				AyakasiAura2 = TrcdAyakasi [0].GetComponent<ayakasi_id> ().ID;
			}

			p1ayakasiA.text = "" + AyakasiAura1;
			p2ayakasiA.text = "" + AyakasiAura2;

			//季節まっちんぐして季節パワー計算（表示）
			SeasonMatch();


			//オーラ計算表示
			sumAura2();


			status = MainStatus.FOUNDITEM1;
			msgText.text = "おそなえカードをひらいてください。";
			return;
		} else {
			msgText.text = "もう一度";
			return;
		}
	}
	//お供え１を読み込む
	void readitem1 ()
	{
		Vector3 ptemp0, ptemp1;
		ptemp0 = new Vector3 (0f, 0f, 0f);
		ptemp1 = new Vector3 (0f, 0f, 0f);
		GameObject[] Trcditem;
		Trcditem = GameObject.FindGameObjectsWithTag ("trackeditem");
		forDebug.text = Trcditem.Length.ToString();
	
	if (Trcditem.Length == 2) {
			item1 = Trcditem [0];
			item2 = Trcditem [1];
			ptemp0 = Camera.main.WorldToViewportPoint (Trcditem [0].transform.position);
			ptemp1 = Camera.main.WorldToViewportPoint (Trcditem [1].transform.position);
			if (ptemp0.x < ptemp1.x) {
				p1item1 = Trcditem [0].name;
				p2item1 = Trcditem [1].name;
				p1ItemId1 = Trcditem [0].GetComponent<ayakasi_id> ().ID;
				p2ItemId1 = Trcditem [1].GetComponent<ayakasi_id> ().ID;
				p1ItemSeason1 = Trcditem [0].GetComponent<ayakasi_id> ().attribute;
				p2ItemSeason1 = Trcditem [1].GetComponent<ayakasi_id> ().attribute;
				
			} else {
				p1item1 = Trcditem [1].name;
				p2item1 = Trcditem [0].name;
				p1ItemId1 = Trcditem [1].GetComponent<ayakasi_id> ().ID;
				p2ItemId1 = Trcditem [0].GetComponent<ayakasi_id> ().ID;
				p1ItemSeason1 = Trcditem [1].GetComponent<ayakasi_id> ().attribute;
				p2ItemSeason1 = Trcditem [0].GetComponent<ayakasi_id> ().attribute;
			}
		
			p1ItemAura1=p1ItemId1;
			p2ItemAura1=p2ItemId1;
			//特殊カード処理
			//コルーチンスタート
			status = MainStatus.NOACTION;
			StartCoroutine("wait4special1");
	
				Trcditem [0].layer = 8;
				Trcditem [1].layer = 8;

				return;
			}
		 else {
			msgText.text = "もう一度";
			return;
		}
	}

	//お供え2を読み込む
	void readitem2 ()
	{
		Vector3 ptemp0, ptemp1;
		ptemp0 = new Vector3 (0f, 0f, 0f);
		ptemp1 = new Vector3 (0f, 0f, 0f);
		GameObject[] Trcditem;
		Trcditem = GameObject.FindGameObjectsWithTag ("trackeditem");
		GameObject[] scnditem = new GameObject[2];
		int i;
		int j;
		int n = 0;
		j = Trcditem.Length;

		if (j < 2 || j > 4) {
			msgText.text = "もう一度";
			return;
		} else {

			for (i=0; i< j; i++) {
				if (Trcditem [i].layer != 8)
				{
					scnditem [n] = Trcditem [i];
					n = n + 1;
				}
			}
		}
		ptemp0 = Camera.main.WorldToViewportPoint (scnditem [0].transform.position);
		ptemp1 = Camera.main.WorldToViewportPoint (scnditem [1].transform.position);
		if (ptemp0.x < ptemp1.x) {
			p1item2 = scnditem [0].name;
			p2item2 = scnditem [1].name;
			p1ItemId2 = scnditem [0].GetComponent<ayakasi_id> ().ID;
			p2ItemId2 = scnditem [1].GetComponent<ayakasi_id> ().ID;
			p1ItemSeason2 = scnditem [0].GetComponent<ayakasi_id> ().attribute;
			p2ItemSeason2 = scnditem [1].GetComponent<ayakasi_id> ().attribute;
		} else {
			p1item2 = scnditem [1].name;
			p2item2 = scnditem [0].name;
			p1ItemId2 = scnditem [1].GetComponent<ayakasi_id> ().ID;
			p2ItemId2 = scnditem [0].GetComponent<ayakasi_id> ().ID;
			p1ItemSeason2 = scnditem [1].GetComponent<ayakasi_id> ().attribute;
			p2ItemSeason2 = scnditem [0].GetComponent<ayakasi_id> ().attribute;

		}
			
		p1ItemAura2=p1ItemId2;
		p2ItemAura2=p2ItemId2;
		//特殊カード処理
		//コルーチン
		status = MainStatus.NOACTION;
		StartCoroutine("wait4special2");


		return;

	}
	//集計処理
	void SumAura ()
	{

		//勝敗決定
		if (aura1 > aura2) {
			p1text.text = p1text.text + "\n" + "WINNER";
			p2text.text = p2text.text + "\n" + "LOSER";

			WinOrLost=1;
			ayakasi1.GetComponent<ayakasi_id>().win=true;
			msgText.text = ayakasi1.name + "のかちです。";

			//オブジェクトにチェック
		} else if (aura2 > aura1) {
			p1text.text = p1text.text + "\n" + "LOSER";
			p2text.text = p2text.text + "\n" + "WINNER";

			WinOrLost=2;
			ayakasi2.GetComponent<ayakasi_id>().win=true;
			msgText.text = ayakasi2.name + "のかちです。";

			//オブジェクトにチェック
		} else {
		
			WinOrLost=0;
		}
	
	
		status = MainStatus.RESULT;
	}

	void sumAura2(){
		aura1 = AyakasiAura1 + sp1 + p1ItemAura1 + p1ItemAura2;
		aura2 = AyakasiAura2 + sp2 + p2ItemAura1 + p2ItemAura2;
	
		AuraText1.text = "" + aura1;
		AuraText2.text = "" + aura2;
	}

	//特殊カード処理
	void special (ref int myaura, ref int youraura, int id, int direction)
	{
		//int tempAura;
		string msg1;

		switch (id) {
		case 1001:

			skip1image.enabled = true;
			oneSkipAnim1 = skip1image.GetComponent<Animation> ();
			if(direction == 0){
			oneSkipAnim1.Play("test_rawimageAnim1");}
			else{oneSkipAnim1.Play("test_rawimageAnim3");
			}
			myaura = myaura - 5;
			Season_val = Season_val + 1;
			if (Season_val > 3) {
				Season_val = Season_val - 4;
			}
			msg1 = "季節が進みます";
			msgText.text = msg1;

			break;
		case 1002:
			//まず演出→コルーチン
			twoskipimage.enabled = true;
			oneSkipAnim1 = twoskipimage.GetComponent<Animation> ();
			if(direction == 0){
				oneSkipAnim1.Play("test_rawimageAnim1");}
			else{oneSkipAnim1.Play("test_rawimageAnim3");
			}
			myaura = myaura - 5;
			Season_val = Season_val + 2;
			if (Season_val > 3) {
				Season_val = Season_val - 4;
			}
		
			msg1 = "季節が進みます";
			msgText.text = msg1;
			//StartCoroutine("wait4enban");
			break;
		case 1003:
			//まず演出→コルーチン
			randomimage.enabled = true;
			oneSkipAnim1 = randomimage.GetComponent<Animation> ();
			if(direction == 0){
				oneSkipAnim1.Play("randomimage_anim");}
			else{oneSkipAnim1.Play("randomimage_anim2");
			}
			Season_val = UnityEngine.Random.Range (0, 4);
			StartCoroutine ("wait4enban2");//ルーレットの後あやかし読み込みへとんじゃうくふうひつよう
		
			break;
	//	case 1004:
	//		myaura = myaura + 10;
	//		youraura = youraura - 10;
	//		break;
	//	case 1005:
	//		//スワップはタイミングで修正の必要あり
	//		myaura = myaura + 30;
	//		tempAura = myaura;
	//		myaura = youraura;
	//		youraura = tempAura;
	//		break;
	//	case 1006:
	//		msg1 = "1枚カードを交換してください。";
	//		msgText.text = msg1;
	//		break;
	//	case 1007:
	//		msg1 = "全てのカードを交換してください。　　";
	//		msgText.text = msg1;
	//		break;
		}

	}
	void SeasonMatch(){
		if (Season_val == p1season) {
			sp1 = 20;
		} else {
			sp1 = 0;
		}
		if (Season_val == p2season) {
			sp2 = 20;
		} else {
			sp2 = 0;
		}
		SeasonAR1.text = "" + sp1;
		SeasonAR2.text = "" + sp2;
	}
	//結果表示処理
	void ShowResult ()
	{
		BattleBackImage.enabled = false;

		switch(Nmatch)
		{
		//１戦目だったら
		case 1:
			if(WinOrLost == 1 ){//プレイヤー１のかち
				p1_result1 = "かち";
				p1_1text.text=p1_result1;
				p2_result1 = "まけ";
				p2_1text.text=p2_result1;
			
				p1_winP = 1;
			}else if(WinOrLost == 2){//プレイヤー2のかち
				p1_result1 = "まけ";
				p1_1text.text=p1_result1;
				p2_result1 = "かち";
				p2_1text.text=p2_result1;
			
				p2_winP = 1;
			}else if(WinOrLost == 0){//ドロー
				p1_result1 = "わけ";
				p1_1text.text=p1_result1;
				p2_result1 = "わけ";
				p2_1text.text=p2_result1;
			}else
			{p1_1text.text="error";}
			msgText.text="第2戦に進みましょう！";


			 
			WinOrLost=0;
			status = MainStatus.BACK; 
			break;
		//２戦目だったら
		case 2:
			if(WinOrLost == 1 ){//プレイヤー１のかち
				p1_result2 = "かち";
				p2_result2 = "まけ";
			
		
				p1_winP = p1_winP + 1;
			}else if(WinOrLost== 2){//プレイヤー2のかち
				p1_result2 = "まけ";
				p2_result2 = "かち";
				p2_winP = p2_winP + 1;
		

			}else if(WinOrLost== 0){//ドロー
				p1_result2 = "わけ";
				p2_result2 = "わけ";
			}
		else
		{p1_2text.text="error";}

			p1_1text.text=p1_result1;
			p1_2text.text=p1_result2;
			p2_1text.text=p2_result1;
			p2_2text.text=p2_result2;

			status = MainStatus.TOTALRESULT;
			if( p1_winP >= 2){
				msgText.text = "東のユーザーの勝ちです！";
			}else if( p2_winP >= 2){
				msgText.text = "西のユーザーの勝ちです！";
			}else{
				status = MainStatus.BACK;
				msgText.text="第3戦に進みましょう！";
				WinOrLost=0;
			}




			break;

		case 3://３戦目だったら
		if(WinOrLost== 1 ){//プレイヤー１のかち
				p1_result3 = "かち";
				p2_result3 = "まけ";
			p1_winP = p1_winP + 1;
	
		}else if(WinOrLost== 2){//プレイヤー2のかち
				p1_result3 = "まけ";
				p2_result3 = "かち";
			p2_winP = p2_winP + 1;
	
		}else if(WinOrLost== 0){//ドロー
				p1_result3 = "わけ";
				p2_result3 = "わけ";
				
		}else{p1_3text.text="error";
		}

			p1_1text.text=p1_result1;
			p1_2text.text=p1_result2;
			p1_3text.text=p1_result3;
			p2_1text.text=p2_result1;
			p2_2text.text=p2_result2;
			p2_3text.text=p2_result3;

		
		
			status = MainStatus.TOTALRESULT;
			if(p1_winP > p2_winP){
				msgText.text = "東のユーザーの勝ちです！";
			}else if(p1_winP < p2_winP){
				msgText.text = "西のユーザーの勝ちです！";
			}else {
				msgText.text = "引き分けでした！";
			}
			break;
			///////////////**************************koko5



		}
	}

	void GoBack () 
	{
		//GameObject[] Trcditem;
		//Trcditem = GameObject.FindGameObjectsWithTag ("trackeditem");
		//Trcditem [0].layer = 3;
		//Trcditem [1].layer = 3;

		item1.layer = 3;
		item2.layer = 3;

		ayakasi1.GetComponent<ayakasi_id>().win=false;
		ayakasi2.GetComponent<ayakasi_id>().win=false;

		Nmatch += 1;
		Season_val += 1;
		p1season = 0;
		p2season = 0;
		player1 = "";
		player2 = "";
		p1item1 = "";
		p1item2 = "";
		p2item1 = "";
		p2item2 = "";
		p1text.text = "";
		p2text.text = "";
		p1ayakasiA.text = "";
		P1Item1A.text = "";
		P1Item2A.text = "";
		p2ayakasiA.text = "";
		P2Item1A.text = "";
		P2Item2A.text = "";
		SeasonAR1.text = "";
		SeasonAR2.text = "";
		p1ItemId1 = 0;
		p1ItemId2 = 0;
		p2ItemId1 = 0;
		p2ItemId2 = 0;

		p1ItemAura1 = 0;
		p1ItemAura2 = 0;
		p2ItemAura1 = 0;
		p2ItemAura2 = 0;
		p1ItemSeason1 = 4; p1ItemSeason2 = 4; p2ItemSeason1 = 4; p2ItemSeason2=4;

		AyakasiAura1 = 0;
		AyakasiAura2 = 0;
		aura1 = 0;
		aura2 = 0;
		sp1 = 0;
		sp2 = 0;

		msgText.text = "";
		if (Season_val>3){
			Season_val=Season_val-4;
		}
		ShowSeason ();
		status = MainStatus.FOUNDAYAKASI;
		msgText.text = "季節は「" + Season + "」になりました！";
	}

	private IEnumerator wait4enban(){
		yield return new WaitForSeconds(2.0f);

		ShowSeason ();
		yield return new WaitForSeconds(2.0f);
		enban.renderer.enabled = false;
		status = MainStatus.FOUNDAYAKASI; 
	}
	private IEnumerator wait4enban2(){

	yield return new WaitForSeconds(1.5f);
	enban.renderer.enabled = true;
	roulette_anime = enban.GetComponent<Animation> ();
	switch (Season_val) {
	case 0:
		roulette_anime.Play ("spring");
		break;
	case 1:
		roulette_anime.Play ("summer");
		break;
	case 2:
		roulette_anime.Play ("autumn");
		break;
	case 3:
		roulette_anime.Play ("winter");
		break;
	}
		yield return new WaitForSeconds(2.0f);

		enban.renderer.enabled = false;
	
	}

	private IEnumerator wait4special1(){
		p1text.text = p1text.text + "\n" + p1item1;
		p2text.text = p2text.text + "\n" + p2item1;

		if (p1ItemId1 > 1000) {
							p1ItemAura1=0;
			
							special(ref p1ItemAura1, ref p2ItemAura1, p1ItemId1,0);
			yield return new WaitForSeconds(2.5f);
			skip1image.enabled = false;
			ShowSeason ();
			msgText.text = Season;
			yield return new WaitForSeconds(0.8f);

						}else if(p1ItemSeason1 == p1season){
							p1ItemAura1 = p1ItemAura1*2;}
			
					
							if (p2ItemId1 > 1000) {
								p2ItemAura1=0;
							special(ref p2ItemAura1, ref p1ItemAura1, p2ItemId1,1);
							//ウエイト
			yield return new WaitForSeconds(2.5f);
			skip1image.enabled = false;
			ShowSeason ();
			msgText.text = Season;
						}else if(p2ItemSeason1 == p2season){
							p2ItemAura1 = p2ItemAura1*2;}
						
			
					//表示
			
					
						P1Item1A.text=""+p1ItemAura1;
						P2Item1A.text=""+p2ItemAura1;
			
						SeasonMatch();
						//オーラ計算表示
						sumAura2();
			
							status = MainStatus.FOUNDITEM2;
							msgText.text = "2枚目のおそなえカードをひらいてください。";
	}
	private IEnumerator wait4special2(){
		
		p1text.text = p1text.text + "\n" + p1item2;
		p2text.text = p2text.text + "\n" + p2item2;
		if (p1ItemId2 > 1000) {
				
			p1ItemAura2 = 0;
			special (ref p1ItemAura2, ref p2ItemAura2, p1ItemId2,0);
			yield return new WaitForSeconds(2.5f);
			skip1image.enabled = false;
			ShowSeason ();
			msgText.text = Season;
			yield return new WaitForSeconds(0.8f);
		
		} else if (p1ItemSeason2 == p1season) {
			p1ItemAura2 = p1ItemAura2 * 2;
		}
	
		if (p2ItemId2 > 1000) {
			p2ItemAura2 = 0;
			special (ref p2ItemAura2, ref p1ItemAura2, p2ItemId2,1);
			//Wait
			yield return new WaitForSeconds(2.5f);
			skip1image.enabled = false;
			ShowSeason ();
			msgText.text = Season;
		} else if (p2ItemSeason2 == p2season) {
			p2ItemAura2 = p2ItemAura2 * 2;
		}
	
	
		P1Item2A.text = "" + p1ItemAura2;
		P2Item2A.text = "" + p2ItemAura2;
			
		SeasonMatch ();
	
		//季節計算（シーズンコンプリート40点加算）
		if (p1season == Season_val && p1season == p1ItemSeason1 && p1season == p1ItemSeason2) {
			//シーズンコンプリートの演出へ
			sp1 = sp1 + 20;

			yield return new WaitForSeconds(1.0f);
			Seasoncomp.enabled=true;
			oneSkipAnim1 = Seasoncomp.GetComponent<Animation> ();
			oneSkipAnim1.Play ("SeasonCompP1");
			yield return new WaitForSeconds(2.0f);
			Seasoncomp.enabled=false;
		}
		if (p2season == Season_val && p2season == p2ItemSeason1 && p2season == p2ItemSeason2) {
				
			sp2 = sp2 + 20;//シーズンコンプリートの演出へ
			yield return new WaitForSeconds(1.0f);
			Seasoncomp.enabled=true;
			oneSkipAnim1 = Seasoncomp.GetComponent<Animation> ();
			oneSkipAnim1.Play ("SeasonCompP2");
			yield return new WaitForSeconds(2.0f);
			Seasoncomp.enabled=false;
		}
		SeasonAR1.text = "" + sp1;
		SeasonAR2.text = "" + sp2;
	
		sumAura2 ();	//オーラ計算表示
	
	
		status = MainStatus.SUMAURA;
		msgText.text = "完了ボタンを押してください";
		bttext.text = "完了";
	}


	public void ResetThisGame () 
	{

		
		item1.layer = 3;
		item2.layer = 3;
		

		p1season = 0;
		p2season = 0;
		player1 = "";
		player2 = "";
		p1item1 = "";
		p1item2 = "";
		p2item1 = "";
		p2item2 = "";
		p1text.text = "";
		p2text.text = "";
		p1ayakasiA.text = "";
		P1Item1A.text = "";
		P1Item2A.text = "";
		p2ayakasiA.text = "";
		P2Item1A.text = "";
		P2Item2A.text = "";
		p1ItemId1 = 0;
		p1ItemId2 = 0;
		p2ItemId1 = 0;
		p2ItemId2 = 0;
		p1ItemAura1 = 0;
		p1ItemAura2 = 0;
		p2ItemAura1 = 0;
		p2ItemAura2 = 0;
		p1ItemSeason1 = 4; p1ItemSeason2 = 4; p2ItemSeason1 = 4; p2ItemSeason2=4;
		sp1 = 0;
		sp2 = 0;
		AyakasiAura1 = 0;
		AyakasiAura2 = 0;
		SeasonAR1.text = "";
		SeasonAR2.text = "";
		aura1 = 0;
		aura2 = 0;
		msgText.text = "";
	
		ShowSeason ();
		status = MainStatus.FOUNDAYAKASI;
		msgText.text = Nmatch+"回戦を再開します。";
	}
	



	public void GameEnd()
	{
		item1.layer = 3;
		item2.layer = 3;
		
		ayakasi1.GetComponent<ayakasi_id>().win=false;
		ayakasi2.GetComponent<ayakasi_id>().win=false;

		p1_1text.text = "";
		p1_2text.text = "";
		p1_3text.text = "";
		p2_1text.text = "";
		p2_2text.text = "";
		p2_3text.text = "";
		AuraText1.text = "";
		AuraText2.text = "";
		Nmatch = 0;
		SeasonIconOff();
	
		////////////////////////koko
		p1_winP = 0;
		p2_winP = 0;
		////////////////////////koko
		p1ayakasiA.text = "";
		P1Item1A.text = "";
		P1Item2A.text = "";
		p2ayakasiA.text = "";
		P2Item1A.text = "";
		P2Item2A.text = "";
		SeasonAR1.text = "";
		SeasonAR2.text = "";
		AyakasiAura1 = 0;
		AyakasiAura2 = 0;
		p1season = 0;
		p2season = 0;
		player1 = "";
		player2 = "";
		p1item1 = "";
		p1item2 = "";
		p2item1 = "";
		p2item2 = "";
		p1text.text = "";
		p2text.text = "";
		p1ItemId1 = 0;
		p1ItemId2 = 0;
		p2ItemId1 = 0;
		p2ItemId2 = 0;
		aura1 = 0;
		aura2 = 0;
		p1ItemAura1 = 0;
		p1ItemAura2 = 0;
		p2ItemAura1 = 0;
		p2ItemAura2=0;
		p1ItemSeason1 = 4; p1ItemSeason2 = 4; p2ItemSeason1 = 4; p2ItemSeason2=4;
		sp1 = 0;
		sp2 = 0;

		msgText.text = "";


		status = MainStatus.TITLE;
	}


	public void spring(){
		Season_val = 0;
		ShowSeason ();
		SeasonMatch ();
		sumAura2 ();
}
	public void summer(){
		Season_val = 1;
		ShowSeason ();
		SeasonMatch ();
		sumAura2 ();
	}
	public void autumn(){
		Season_val = 2;
		ShowSeason ();
		SeasonMatch ();
		sumAura2 ();
	}
	public void winter(){
		Season_val = 3;
		ShowSeason ();
		SeasonMatch ();
		sumAura2 ();
	}

}
