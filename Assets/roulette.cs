﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class roulette : MonoBehaviour {
	public Button MainBt;
	public GameObject enban;
	public Animation roulette_anime;
	public int Season_val;
	
	
	// Use this for initialization
	void Start () {
		roulette_anime = enban.GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void ButtonClick(){
		Season_val = UnityEngine.Random.Range (0, 4);
		switch (Season_val) {
		case 0:
			roulette_anime.Play ("spring");
			break;
		case 1:
			roulette_anime.Play ("test");
			break;
		case 2:
			roulette_anime.Play ("autumn");
			break;
		case 3:
			roulette_anime.Play ("winter");
			break;
			
		}
		
		
	}
	
}
