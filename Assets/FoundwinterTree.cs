﻿using UnityEngine;
using System.Collections;

public class FoundwinterTree : MonoBehaviour {

	public bool Onwinter;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if ((this.tag=="trackedTree")&(Onwinter==true)) {
			renderer.enabled = true;
			OnTrackingFound();
			
		} 
		else
		{
			renderer.enabled=false;
			OnTrackingLost();
			
		}
	}
	
	private void OnTrackingFound()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		
		
		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}
		
		
		
	}
	private void OnTrackingLost()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}
		
		
	}
}
